## OOP & OOD
**O**bject-**O**riented **P**rogramming & **O**bject-**O**riented **D**esign.

### OOP Encapsulation

> **Encapsulation** is

...the result of hiding a representation and implementation in an object.
The representation is not visible and cannot be accessed directly from outside the
object. Operations are the only way to access and modify an object's representation.

#### Data Encapsulation
One of the hallmarks of object-oriented design and programming is *data encapsulation*.

> **Data encapsulation** means

...that your type’s fields should never be publicly exposed because it’s too easy to write code
that improperly uses the fields, corrupting the object’s state.

#### Memento Pattern
Without violating *encapsulation*, capture and externalize an object's internal state
so that the object can be restored to this state later.

##### Role
This pattern is used to capture an object’s internal state and save it externally so that
it can be restored later.

##### Memento Pattern class diagram
![](https://bitbucket.org/raman-m/computer-science-topics/raw/64550a1a444704e63fe43b06f8c6ba35d13f087b/OOP/Memento_Pattern.svg)

### OOD Basics
[Basics](Basics.md)
